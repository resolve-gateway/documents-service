FROM openjdk:11.0.2-jdk-slim-stretch
ARG JAR_FILE
ADD target/${JAR_FILE} app.jar

EXPOSE 8081

ENTRYPOINT ["java", "-jar", "/app.jar"]
