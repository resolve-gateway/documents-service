package com.vaddya.documents.hateoas;

import com.vaddya.documents.config.EndpointsConfig;
import com.vaddya.documents.domain.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

@Component
public class DocumentResourceAssembler implements ResourceAssembler<Document, Resource<Document>> {

    private final String personsURL;

    @Autowired
    public DocumentResourceAssembler(EndpointsConfig endpoints) {
        this.personsURL = endpoints.getPersons();
    }

    @Override
    public Resource<Document> toResource(Document document) {
        return new Resource<>(document, getPersonLink(document.getPersonId()));
    }

    private Link getPersonLink(String id) {
        return new Link(personsURL + "/persons/" + id, "person");
    }
}
