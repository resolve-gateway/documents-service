package com.vaddya.documents.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Data
public class EndpointsConfig {

    @Value("${endpoints.persons}")
    private String persons;
}
