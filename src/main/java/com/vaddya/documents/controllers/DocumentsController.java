package com.vaddya.documents.controllers;

import com.vaddya.documents.domain.Document;
import com.vaddya.documents.hateoas.DocumentResourceAssembler;
import com.vaddya.documents.service.DocumentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(
        value = "/documents",
        produces = MediaType.APPLICATION_JSON_VALUE
)
public class DocumentsController {
    private final DocumentsService service;
    private final DocumentResourceAssembler resourceAssembler;

    @Autowired
    public DocumentsController(DocumentsService service, DocumentResourceAssembler resourceAssembler) {
        this.service = service;
        this.resourceAssembler = resourceAssembler;
    }

    @GetMapping
    public ResponseEntity<PagedResources> getDocuments(Pageable pageable,
                                                       PagedResourcesAssembler<Document> pageAssembler) {
        Page<Document> documents = service.getDocuments(pageable);
        return new ResponseEntity<>(pageAssembler.toResource(documents, resourceAssembler), HttpStatus.OK);
    }

    @GetMapping("/{documentId}")
    public ResponseEntity<Resource<Document>> getDocument(@PathVariable Integer documentId) {
        return new ResponseEntity<>(resourceAssembler.toResource(service.getDocument(documentId)), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity createDocument(@RequestBody Document document) {
        service.createDocument(document);
        return new ResponseEntity(HttpStatus.OK);
    }
}
