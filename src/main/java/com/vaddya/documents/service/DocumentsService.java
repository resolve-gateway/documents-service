package com.vaddya.documents.service;

import com.vaddya.documents.config.EndpointsConfig;
import com.vaddya.documents.domain.Document;
import com.vaddya.documents.repository.DocumentsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class DocumentsService {

    private final DocumentsRepository repository;

    @Autowired
    public DocumentsService(DocumentsRepository repository, EndpointsConfig endpoints) {
        this.repository = repository;
    }

    public Page<Document> getDocuments(Pageable pageable) {
        return repository.findAll(pageable);
    }

    public void createDocument(Document document) {
        repository.save(document);
    }

    public Document getDocument(Integer documentId) {
        return repository.findById(documentId).orElseThrow();
    }
}
