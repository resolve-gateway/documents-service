package com.vaddya.documents.repository;

import com.vaddya.documents.domain.Document;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentsRepository extends PagingAndSortingRepository<Document, Integer> {

}
